notes 05 march 2023

1.in any instance
vim docker file to create httpd server
---------------------------------------
# Use an existing httpd image as the base
FROM httpd:latest

# Copy your local website files to the image
#COPY ./website/ /usr/local/apache2/htdocs/

# Expose port 80 to allow incoming HTTP requests
EXPOSE 80


--------------------------------------
2.write jenkins file to build and tag image



docker build image

docker run 
------------------------------------------------------
pipeline {
    agent {
       label "docker"
    }
    stages {
	    stage(Code_Checkout){
		    steps{
			   git branch: 'main', credentialsId: 'git', url: 'https://gitlab.com/srustikgowda/build-docker-image.git'
			}
		}
        stage('Build_Docker_Image') {
            steps {
                sh 'sudo docker build -t my-httpd .'
            }
        }
        stage('Docker_Tag') {
            steps {
                sh 'sudo docker tag my-httpd srustikgowda/my-httpd:$tag'
            }
        }
		stage('Docker Login'){
		    steps{
			    withCredentials([usernamePassword(credentialsId: 'docker-login', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
						sh """ sudo docker login -u=$USER -p=$PASS """
				}			
			}
		}
        stage('Docker_push') {
            steps {
				sh 'echo Push Docker Image'
				sh 'sudo docker push srustikgowda/my-httpd:$tag'
            }
        }
    }
}


--------------------------
3.dockerfile and jenkins file upload to gitlab
----------------------------

4.got0 tojenkins dashboard
 jenkins master(ensure java and docker is installed)

5.got tojenkins dashboard
goto jenkis dashboard  (add node as slve docker) ensure
)

build now 

 to generate docker image for tag pusheed to dockerhub as per release version
============ 
6.docker compose

7.in slave node( createw new instance)
install docker
install java, git

install docker compose by using below commands

#docker --version
#sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
#sudo chmod +x /usr/local/bin/docker-compose
#docker-compose --version
----------------------
8.ensure docer-compose.yml and jenkinsfile for deplouying are uploaded to gitlab along with project
----------------------------------------------

69.docker-compose.yml

version: '3'

services:
  product-service:
    build: ./product
    volumes:
      - ./product:/usr/src/app
    ports:
      - 5001:80

  website:
    image: php:apache
    volumes:
      - ./website:/var/www/html
    ports:
      - 5000:80
    depends_on:
      - product-service

-----------------------------------------------
10.jenkinsfile

pipeline {
    agent {
        label "deploy"
    }
    stages {
        stage('code_checkout') {
            steps {
                git branch: 'main', credentialsId: 'git', url: 'https://gitlab.com/mohannaik/docker_jenkins_httpd.git'
            }
        }
        stage('deploy') {
            steps {
               sh 'sudo docker-compose up -d' 
            }
        }        
    }
}
----------------------------------------

11.goto jenkis dashboard

add the deploy node to jenkins

12.create new job to deploy docker job
 then 
build now

